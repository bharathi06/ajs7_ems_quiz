// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// TODO Remove referance data
// var config={};
// var socketBaseUrl='ws://52.66.23.63:8070';
// var ApiBaseUrl='https://13.232.163.191';
// var s3BaseUrl = 'http://s3.ap-south-1.amazonaws.com/devqa-emsdata';
// var Api = {
//     'ws_Get_Question':socketBaseUrl+'/sony_kbc',
//     'send_Answers':'http://13.232.78.161:8070/api/sendAnswers',
//     'top_Ranks':s3BaseUrl+"/leaderboard/prog-3/topranking/leader.json",
//     'get_Profile':s3BaseUrl+'/leaderboard/prog-3/profiledetails/ProfileDetails',
//     'offline':ApiBaseUrl+'/api/offline',
//     'offline':s3BaseUrl+"/offline/offline-",
//     'winningItems':s3BaseUrl+'/winning/items.json',
//     'jackpot':s3BaseUrl+"/jackpot/jackpot",
//     'languageSelectInfo':'https://13.232.163.191:8095/api/v1/user/languageSelectInfo',
//     'getDailyScore':'https://dev-emssdk.sonyliv.com/api/v1/webuser/getDailyRewardPointsForKBC/'

// }
export const environment = {
  production: false,
  socketBaseUrl: 'ws://52.66.23.63:8070',
  s3BaseURL: 'https://s3.ap-south-1.amazonaws.com',
  channel: '/sony_kbc'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
// https://s3.ap-south-1.amazonaws.com/devqa-emsdata/newkbc/config.json
export const CONFIG_LINK = {
  // TODO INFO Old link https://s3.ap-south-1.amazonaws.com/devqa-emsdata/newkbc/config.json 
  BASE_URL: 'http://s3.ap-south-1.amazonaws.com/devqa-emsdata/kbc/config',
  FILE_COMMON_FORMATE: '/config_',
  FILE_TYPE: '.json'
};

