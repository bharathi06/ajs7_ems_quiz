import { QuestionService } from './question.service';
import { Injectable } from '@angular/core';
import { QuizService } from './quiz.service';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AnswerService {
  messages: Subject<any>;
  constructor(private qS: QuizService) {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    // this.messages = <Subject<any>>qS
    //   .connect()
    //   .map((response: any): any => {
    //     return response;
    //   });
  }
  sendMessage(msg) {
    this.messages.next(msg);
  }
}
