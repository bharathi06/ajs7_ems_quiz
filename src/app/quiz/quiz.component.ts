import { DataService } from './../data.service';
import { Option } from './../model/quiz.model';
import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { Observable } from 'rxjs';

import * as io from 'socket.io-client';
import { environment } from '../../environments/environment';
import { ConfigService } from '../config.service';
export interface ImageSetting {
  background?: string;
  border?: string;
  borderColor?: string;
}
@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  payload : any;
  endPoint = environment.socketBaseUrl + environment.channel;
  socketX: any;
  oxygen: any;
  question: string;
  answer: any;
  options: any = {};
  svgImage: ImageSetting = {};
  initialCounter = 20;
  currectAnswer: any;
  activeInterval: false;
  showSections: any = {
    answer: false,
    question: false,
    waiting: false,
    options: false
  };
  // TODO Take it from env file
  activeColor = 'option-background-active';
  inactiveColor = 'option-background-inactive';
  greenColor = 'option-background-green';
  showColor: any = {
    A: '',
    B: '',
    C: '',
    D: ''
  };
  optionColor: any = [];
  currentLanguage: string;
  constructor(
    private elementRef: ElementRef,
    private configService: ConfigService,
    private dataService: DataService,
    private renderer: Renderer2) {
    // tslint:disable-next-line: curly
    for (let i = 0; i < 4; i++) this.optionColor.push(this.showColor);
  }

  ngOnInit() {


    const channel = this.dataService.getExperience();
    this.currentLanguage = this.dataService.getLanguage();


    this.payload = {
      query: 'userid=' + 1234 + '&language=' + this.currentLanguage,
      transports: ['websocket'],
      upgrade: false
    };
    console.log(this.payload);
    
    this.renderer.setStyle(this.elementRef.nativeElement.ownerDocument.body, 'background-color', '#004064');
    this.svgImage.borderColor = '#ffcd4a';
    this.svgImage.border = '2px';
    // console.log(this.socketX.connect());
    let url = environment.socketBaseUrl + '/' + channel;
    this.socketX = io(url, this.payload);
    this.socketX.on('connect', (res) => {
      console.log('Connected!!');

      this.socketX.emit('get_current_question_' + this.currentLanguage , '');
      this.socketX.emit('get_current_options_' + this.currentLanguage, '');
    });
    this.socketX.on('question', (res: any) => {
      this.question = res.question;
      this.showSections.option = false;
    });
    this.socketX.on('get_current_question_' + this.currentLanguage, (res: any) => {
      this.question = res.question;
      this.showSections.question = true;
    });
    // Answer
    this.socketX.on('get_current_answer_' + this.currentLanguage, (res: any) => {
      console.log(res);
    });
    this.socketX.on('answer', (res: any) => {
      console.log(res);
      // this.options = {};
      this.showSections.answer = true;
      this.showColor[res.correct_option] = 'option-background-green';

    });

    // Options
    this.socketX.on('options', (res: any) => {
      console.log(res);
      this.showSections.option = true;
      this.options.a = res.option_a;
      this.options.b = res.option_b;
      this.options.c = res.option_c;
      this.options.d = res.option_d;
    });
    this.socketX.on('get_current_options_' + this.currentLanguage, (res: any) => {
      console.log('get_current_options_' + this.currentLanguage, res);
      this.showSections.option = true;
      this.options.a = res.option_a;
      this.options.b = res.option_b;
      this.options.c = res.option_c;
      this.options.d = res.option_d;
    });
    // Waiting
    this.socketX.on('waiting', (res: any) => {
      console.log(res);
      this.showSections = {};
    });

    this.socketX.on('disconnect', (res: any) => {
      console.log(res);
    });

    this.socketX.on('error', (res: any) => {
      console.log(res);
    });

    this.socketX.on('connect_failed', (res: any) => {
      console.log('Sorry, there seems to be an issue with the connection!');
    });
  }

  public updateAnswer(data: string) {
    this.resetColor();
    this.showColor[data] = 'option-background-active';
  }
  resetColor() {
    this.showColor = {
      A: '',
      B: '',
      C: '',
      D: ''
    };
  }

  lockAnswer() {
    this.initialCounter = 0;
  }

  sendAnswer() {
  }

  //    wrap(text) {
  //     text.each(function() {
  //         var text = d3.select(this);
  //         var words = text.text().split(/\s+/).reverse();
  //         var lineHeight = 20;
  //         var width = parseFloat(text.attr('width'));
  //         var y = parseFloat(text.attr('y'));
  //         var x = text.attr('x');
  //         var anchor = text.attr('text-anchor');

  //         var tspan = text.text(null).append('tspan').attr('x', x).attr('y', y).attr('text-anchor', anchor);
  //         var lineNumber = 0;
  //         var line = [];
  //         var word = words.pop();

  //         while (word) {
  //             line.push(word);
  //             tspan.text(line.join(' '));
  //             if (tspan.node().getComputedTextLength() > width) {
  //                 lineNumber += 1;
  //                 line.pop();
  //                 tspan.text(line.join(' '));
  //                 line = [word];
  //                 tspan = text.append('tspan').attr('x', x).attr('y', y + lineNumber * lineHeight).attr('anchor', anchor).text(word);
  //             }
  //             word = words.pop();
  //         }
  //     });
  // }


  // getBackground() {
  //   let bg = 'data:image/svg+xml;utf8,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAiIHZpZXdCb3g9IjAgMCAzMDAgMTAwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIj4KICAgICA8bGluZSB4MT0iMCIgeTE9IjUwIiB4Mj0iMzAwIiB5Mj0iNTAiIFthdHRyLnN0cm9rZS13aWR0aF09InN2Z0ltYWdlLmJvcmRlciIKICAgICAgIFthdHRyLnN0cm9rZV09InN2Z0ltYWdlLmJvcmRlckNvbG9yIiAvPgoKICAgICA8ZGVmcz4KICAgICAgIDxyYWRpYWxHcmFkaWVudCBpZD0iZ3JhZDEiIGN4PSI1MCUiIGN5PSI1MCUiIHI9IjUwJSIgZng9IjUwJSIgZnk9IjUwJSI+CiAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0eWxlPSJzdG9wLWNvbG9yOnJnYigxMDMsMzAsMTYwKTsKICAgICAgICAgc3RvcC1vcGFjaXR5OjEiIC8+CiAgICAgICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3R5bGU9InN0b3AtY29sb3I6cmdiKDU1LDAsMTAxKTtzdG9wLW9wYWNpdHk6MSIgLz4KICAgICAgIDwvcmFkaWFsR3JhZGllbnQ+CiAgICAgPC9kZWZzPgogICAgIDxwb2x5Z29uIGZpbGw9InVybCgjZ3JhZDEpIiBbYXR0ci5zdHJva2Utd2lkdGhdPSJzdmdJbWFnZS5ib3JkZXIiIFthdHRyLnN0cm9rZV09InN2Z0ltYWdlLmJvcmRlckNvbG9yIgogICAgICAgcG9pbnRzPSIxMCw1MCA1MCwwLCAyNTAsMCAyOTAsNTAsIDI1MCwxMDAgNTAsMTAwIiAvPgogICAgIDwhLS0gPHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMTUwIiB5PSI1MCIgZmlsbD0iI2ZmZiI+IHt7IHF1ZXN0aW9uIH19IDwvdGV4dD4gLS0+CiAgIDwvc3ZnPg=';
  //   let questionBackground = this.renderer.selectRootElement('p');
  //   this.renderer.setStyle(questionBackground, 'background-image', bg);
  // }

}
