export class OptionData {
    id: string;
    name: string;
    value: string;
    questionId: string;
    channel: string;
    type: string;
}