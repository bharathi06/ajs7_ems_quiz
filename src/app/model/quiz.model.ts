export class QuizModel {
    question: string;
    answer: string;
    options: Array<Option>;
}
export class Option {
    id: number;
    title: string;
}
