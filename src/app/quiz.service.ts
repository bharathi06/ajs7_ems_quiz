import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as Rx from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class QuizService {


  private socket;
  constructor() { }
  connect(): Rx.Subject<MessageEvent> {
    this.socket = io(environment.socketBaseUrl);
    // FIXME below tslint error
    // tslint:disable-next-line: no-shadowed-variable
    const observable = new Observable(observer => {
      this.socket.on('message', (data) => {
        console.log('Received Message');
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    const observer = {
      next: (data: any) => {
        this.socket.emit('message', JSON.stringify(data));
      }
    }
    return Rx.Subject.create(observer, observable);
  }
}
