/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { CounterDirective } from './counter.directive';

describe('Directive: Counter', () => {
  it('should create an instance', () => {
    const directive = new CounterDirective();
    expect(directive).toBeTruthy();
  });
});
