import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private myValue: any;
  private status = false;
  private experience = '';
  private experienceId: number;
  private currentLanguage = 'english';
  constructor(private cS: ConfigService) { }

  setValue(val: any) {
    this.myValue = val;
    // TODO make it dynamic
    // this.myValue.currentLanguage = 'english';

    // TODO testing
    this.setExperience(val.socket_connection);
    this.setLanguage(val.current_language);
    this.status = true;
  }

  getValue() {
    return this.myValue;
  }
  getStatus() {
    return this.status;
  }
  setExperience(data: string) {
    this.experience = data;
  }
  getExperience() {
    return this.experience;
  }
  getLanguage() {
    return this.currentLanguage;
  }
  setLanguage(data: string) {
    this.currentLanguage = data;
  }
  setExperienceId(data: number) {
    this.experienceId = data;
  }
  getExperienceId() {
    return this.experienceId;
  }
}