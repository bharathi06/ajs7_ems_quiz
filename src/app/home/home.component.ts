import { ConfigService } from './../config.service';
import { Component, OnInit, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  heroImage: string;
  mainLogo: string;
  mainLogoWidth: string;
  bgColor: string;
  loadingData = false;
  config: any;
  showScreen = false;
  currentLanguage: string;
  animal: string;
  customConfig: any = {};
  language: any;
  languageDisplay: any;
  experienceId: number;
  constructor(
    private elementRef: ElementRef,
    private configService: ConfigService,
    private renderer: Renderer2,
    private dataService: DataService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    // TODO remove below code
    const cl = this.dataService.getLanguage();
    cl ? this.currentLanguage = cl : this.currentLanguage = 'english';
    // TODO Test with dynamic thing
    const eId = this.dataService.getExperienceId();
    if (eId) {
      this.experienceId = eId;
    } else {
      this.experienceId = this.route.snapshot.queryParams['experienceId'];
      this.dataService.setExperienceId(this.experienceId);
    }
    console.log(this.experienceId);
    this.renderer.setStyle(this.elementRef.nativeElement.ownerDocument.body, 'background-image', 'url(http://devqa-emsstatic.s3-website.ap-south-1.amazonaws.com/kbcquestion/images/water-mark-logo.png)');
    this.mainLogoWidth = '80';
    this.bgColor = '482a6d';
    this.dataService.getStatus() ? this.getSereenData() : this.config = this.setScreen();
  }

  switchLanguage() {
    if (this.currentLanguage === this.language[0].value) {
      this.languageDisplay = this.language[1].display;
      this.currentLanguage = this.language[1].value;

    } else {
      this.currentLanguage = this.language[0].value;
      this.languageDisplay = this.language[0].display;

    }
    this.customConfig = this.config[this.currentLanguage].home;
    this.dataService.setLanguage(this.currentLanguage);
  }
  ngAfterViewInit(): void {
  }

  setScreen() {
    this.configService.getConfigData(this.experienceId)
      .subscribe((data: any) => {
        this.config = data;
        this.currentLanguage = data.current_language;
        this.customConfig = this.config[this.currentLanguage].home;
        console.log(this.customConfig);
        this.language = data.language;
        this.dataService.setValue(data);
        this.dataService.setExperience(data.socket_connection);
        this.showScreen = true;
      });
    this.dataService.setLanguage(this.currentLanguage);
    this.dataService.setExperienceId(this.experienceId);
  }
  getSereenData() {
    this.config = this.dataService.getValue();
    this.currentLanguage = this.dataService.getLanguage();
    this.customConfig = this.config[this.currentLanguage].home;
    this.language = this.config.language;
    this.renderer.setStyle(this.elementRef.nativeElement.ownerDocument.body, 'backgroundImage', this.heroImage);
    this.showScreen = true;
  }
}
