import { DataService } from './data.service';
import { Component, OnInit } from '@angular/core';
import { ConfigService } from './config.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private configService: ConfigService,
    private dataService: DataService
  ) { }
  ngOnInit() {
    // this.setConfigData();
    // this.dataService.getValue();
  }
  setConfigData() {
    this.configService.getConfigData(3)
      .subscribe((data: any) => {
        this.dataService.setValue(data);
      });
  }
}
