import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import { NotFoundComponent } from './NotFound/NotFound.component';
import { QuizComponent } from './quiz/quiz.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { PrizesComponent } from './prizes/prizes.component';
import { CounterDirective } from './counter.directive';
@NgModule({
   declarations: [
      AppComponent,
      HomeComponent,
      NotFoundComponent,
      QuizComponent,
      LeaderboardComponent,
      PrizesComponent,
      CounterDirective
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      NgbModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
