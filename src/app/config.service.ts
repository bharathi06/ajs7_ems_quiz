import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG_LINK } from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ConfigService {
constructor(private http: HttpClient) { }
  getConfigData(episodeId: number) {
    return this.http.get(CONFIG_LINK.BASE_URL + CONFIG_LINK.FILE_COMMON_FORMATE + episodeId + CONFIG_LINK.FILE_TYPE);
  }
}
