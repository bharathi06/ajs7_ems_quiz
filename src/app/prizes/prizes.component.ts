import { Component, OnInit } from '@angular/core';
import { ConfigService } from './../config.service';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-prizes',
  templateUrl: './prizes.component.html',
  styleUrls: ['./prizes.component.css']
})
export class PrizesComponent implements OnInit {
  list: any;
  experienceId: number;
  config: any;
  color: string = '#fff';
  heading: string = 'Prizes to win';
  customConfig: any = {};
  currentLanguage: string;
  constructor(
    private configService: ConfigService,
    private dataService: DataService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const cl = this.dataService.getLanguage();
    cl ? this.currentLanguage = cl : this.currentLanguage = 'english';
    // this.list = [{name: 'First Prize', url: '#', image:''}, {name: 'Second Prize', url: '#', image:''}, {name: 'Third Prize', url: '#', image:''}]
    const eId = this.dataService.getExperienceId();
    if (eId) {
      this.experienceId = eId;
    } else {
      this.experienceId = this.route.snapshot.queryParams['experienceId'];
      this.dataService.setExperienceId(this.experienceId);
    }
    this.configService.getConfigData(this.experienceId)
      .subscribe((data: any) => {
        this.config = data;
        this.customConfig = this.config[this.currentLanguage].prizes_to_win;
        console.log(this.customConfig);
      });
  }

}
