import { AppComponent } from './app.component';
import { QuizComponent } from './quiz/quiz.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/home/home.component';
import { NotFoundComponent } from './NotFound/NotFound.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { PrizesComponent } from './prizes/prizes.component';

const routes: Routes = [
  { path: 'home/:id', component: HomeComponent },
  { path: 'quiz', component: QuizComponent},
  { path: 'leaderboard', component: LeaderboardComponent},
  { path: 'prizes', component: PrizesComponent},
  { path: '404', component: NotFoundComponent},
  { path: '', component: HomeComponent },
  {path: '**', redirectTo: '/404'},
];

@NgModule({
   imports: [
      RouterModule.forRoot(routes)
   ],
   exports: [
      RouterModule
   ],
   declarations: [
   ]
})
export class AppRoutingModule { }
